package com.hogwarts.aitest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class AitestApplication {

	@GetMapping("/index")
	public String index() {
		return "第一个例子";
	}

	public static void main(String[] args) {
		SpringApplication.run(AitestApplication.class, args);
	}
}
